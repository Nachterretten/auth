package client

import (
	"context"
	"fmt"
	"gitlab.com/Nachterretten/json-rpc/config"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"sync"
)

type ClientGRPC struct {
	conf   config.RPCClient
	logger *zap.Logger
	client *grpc.ClientConn
	sync.Mutex
}

func (c *ClientGRPC) Invoke(ctx context.Context, method string, args interface{}, reply interface{}, opts ...grpc.CallOption) error {
	return c.client.Invoke(ctx, method, args, reply, opts...)
}

func (c *ClientGRPC) NewStream(ctx context.Context, desc *grpc.StreamDesc, method string, opts ...grpc.CallOption) (grpc.ClientStream, error) {
	return c.client.NewStream(ctx, desc, method, opts...)
}

func NewClientGRPC(conf config.RPCClient, logger *zap.Logger) *ClientGRPC {
	client, err := grpc.Dial(fmt.Sprintf("%s:%s", conf.Host, conf.Port), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		logger.Fatal("grpc not connection")
	}
	logger.Info("grpc connected")
	return &ClientGRPC{conf: conf, logger: logger, client: client}
}
