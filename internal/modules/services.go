package modules

import (
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/client"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/component"
	aservice "gitlab.com/Nachterretten/json-rpc/internal/modules/auth/service"
	uservice "gitlab.com/Nachterretten/json-rpc/internal/modules/user/service"
	"gitlab.com/Nachterretten/json-rpc/internal/storages"
	"gitlab.com/Nachterretten/json-rpc/rpc/gRPC/proto_user"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServiceJRPC(storages *storages.Storages, components *component.Components) *Services {
	userRPC := client.NewJSONRPC(components.Conf.UserRPC, components.Logger)
	userServiceRPC := uservice.NewUserServiceJSONRPC(userRPC)
	return &Services{
		User: userServiceRPC,
		Auth: aservice.NewAuth(userServiceRPC, storages.Verify, components),
	}
}

func NewServicesGRPC(storages *storages.Storages, components *component.Components) *Services {
	userGRPS := client.NewClientGRPC(components.Conf.UserRPC, components.Logger)
	userGRPSClient := proto_user.NewUserServiceRPCClient(userGRPS)
	userService := uservice.NewExchangeServiceGRPC(userGRPSClient)

	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
