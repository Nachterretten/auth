package gRPC

import (
	"context"
	"gitlab.com/Nachterretten/json-rpc/internal/modules/auth/service"
	"gitlab.com/Nachterretten/json-rpc/rpc/gRPC/proto_auth"
)

type AuthServiceGRPC struct {
	auth service.Auther
}

func (a *AuthServiceGRPC) Register(ctx context.Context, in *proto_auth.RegisterIn) (*proto_auth.RegisterOut, error) {
	out := a.auth.Register(ctx, service.RegisterIn{
		Email:    in.GetEmail(),
		Password: in.GetPassword(),
	})
	return &proto_auth.RegisterOut{
		Status:    int32(out.Status),
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (a *AuthServiceGRPC) AuthorizeEmail(ctx context.Context, in *proto_auth.AuthorizeEmailIn) (*proto_auth.AuthorizeEmailOut, error) {
	out := a.auth.AuthorizeEmail(ctx, service.AuthorizeEmailIn{
		Email:          in.GetEmail(),
		Password:       in.GetPassword(),
		RetypePassword: in.GetRetypePassword(),
	})

	return &proto_auth.AuthorizeEmailOut{
		UserId:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}

func (a *AuthServiceGRPC) AuthorizeRefresh(ctx context.Context, in *proto_auth.AuthorizeRefreshIn) (*proto_auth.AuthorizeOut, error) {
	out := a.auth.AuthorizeRefresh(ctx, service.AuthorizeRefreshIn{
		UserID: int(in.GetUserId()),
	})

	return &proto_auth.AuthorizeOut{
		UserId:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}

func (a *AuthServiceGRPC) AuthorizePhone(ctx context.Context, in *proto_auth.AuthorizePhoneIn) (*proto_auth.AuthorizeOut, error) {
	out := a.auth.AuthorizePhone(ctx, service.AuthorizePhoneIn{
		Phone: in.GetPhone(),
	})

	return &proto_auth.AuthorizeOut{
		UserId:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}

func (a *AuthServiceGRPC) SendPhoneCode(ctx context.Context, in *proto_auth.SendPhoneCodeIn) (*proto_auth.SendPhoneCodeOut, error) {
	out := a.auth.SendPhoneCode(ctx, service.SendPhoneCodeIn{
		Phone: in.GetPhone(),
	})

	return &proto_auth.SendPhoneCodeOut{
		Phone: out.Phone,
		Code:  int32(out.Code),
	}, nil
}

func (a *AuthServiceGRPC) VerifyEmail(ctx context.Context, in *proto_auth.VerifyEmailIn) (*proto_auth.VerifyEmailOut, error) {
	out := a.auth.VerifyEmail(ctx, service.VerifyEmailIn{
		Hash:  in.GetHash(),
		Email: in.GetEmail(),
	})

	return &proto_auth.VerifyEmailOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func NewAutherServiceJSONGRPC(auth service.Auther) *AuthServiceGRPC {
	return &AuthServiceGRPC{auth: auth}
}
